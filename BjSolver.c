/* Simple solver */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAX_STACK 100

int stack_count;
int stacks[MAX_STACK];
int target[MAX_STACK];
int pos = 0;

bool compStacks(void)
{
    bool matches = true;
    int i;
    for (i = 0; i < stack_count && matches == true; i++)
    {
        if (stacks[i] != target[i]) matches = false;
    }
    
    return matches;
}

int findNearestLessThan(void)
{
    int nearestRightPos = -1;
    int nearestLeftPos = -1;
    int i;
    
    /* See if we're already on one */
    if (stacks[pos] < target[pos]) return pos;
    
    /* Check to the right */
    for (i = pos + 1; i < stack_count; i++)
    {
        if (stacks[i] < target[i]) {
            nearestRightPos = i;
            break;
        }
    }
    
    /* Check to the left */
    for (i = pos - 1; i >= 0; i--)
    {
        if (stacks[i] < target[i]) {
            nearestLeftPos = i;
            break;
        }
    }
    
    /* If we didn't find on one side return the other */
    if (nearestRightPos == -1) return nearestLeftPos;
    if (nearestLeftPos == -1) return nearestRightPos;
    
    /* Compare and return the one with the least difference */
    return (pos - nearestLeftPos < nearestRightPos - pos) ? nearestLeftPos : nearestRightPos;
}

int findNearestGreaterThan(int lessThanPos)
{
    int nearestRightPos = -1;
    int nearestLeftPos = -1;
    int i;
    
    /* Check to the right */
    for (i = lessThanPos + 1; i < stack_count; i++)
    {
        if (stacks[i] > target[i]) {
            nearestRightPos = i;
            break;
        }
    }
    
    /* Check to the left */
    for (i = lessThanPos - 1; i >= 0; i--)
    {
        if (stacks[i] > target[i]) {
            nearestLeftPos = i;
            break;
        }
    }
    
    /* If we didn't find on one side return the other */
    if (nearestRightPos == -1) return nearestLeftPos;
    if (nearestLeftPos == -1) return nearestRightPos;
    
    /* Compare and return the one with the least difference */
    return (lessThanPos - nearestLeftPos < nearestRightPos - lessThanPos) ? nearestLeftPos : nearestRightPos;
}

void moveToPos(int newPos)
{
    int i;
    int origPos = pos;
    
    /* If we need to move left */
    if (newPos <= pos) {
        for (i = 0; i < origPos - newPos; i++) {
            printf("L\n");
            if (pos > 0) pos--;
        }
    }
    
    /* If we need to move right */
    else {
        for (i = 0; i < newPos - origPos; i++) {
            printf("R\n");
            if (pos < stack_count - 1) pos++;
        }
    }
}

int main(int argc, char *argv[])
{
    int i;
    bool holding = false;
    
    /* The test file we're solving for */
    FILE *fp = NULL;
    
    if (argc != 2) {
        printf("Usage: %s <testfile>\n", argv[0]);
        return 0;
    }
    
    if (!(fp = fopen(argv[1], "r"))) {
        printf("Unable to open file: %s\n", argv[1]);
        return 0;
    }
    
    /* Code copied from judging program */
    fscanf(fp, "%d", &stack_count);

    for (i = 0; i < stack_count; i++) {
        fscanf(fp, "%d", &(stacks[i]));
    }

    for (i = 0; i < stack_count; i++) {
        fscanf(fp, "%d", &(target[i]));
    }
    
    fclose(fp);
    /************************************/
    
    do {
        
        /* Find the nearest location with less than desired number of blocks */
        int nearestLessThan = findNearestLessThan();
        
        /* Find the location closest to the above less than that is greater than desired */
        int nearestGreaterThan = findNearestGreaterThan(nearestLessThan);
        
        /* Move to the closest greater than */
        moveToPos(nearestGreaterThan);
        
        /* Pick up a block */
        printf("P\n");
        if (stacks[pos] > 0) {
            stacks[pos]--;
            holding = true;
        }
        else {
            printf("ERROR - picking up stack with no blocks!\n");
            printf("X\n");
            return -1;
        }
        
        /* Move to the less than */
        moveToPos(nearestLessThan);
        
        /* Drop the block */
        printf("D\n");
        if (holding) {
            stacks[pos]++;
            holding = false;
        }
        
        //getchar();
        
    } while(!compStacks());

    /* Finished */
    printf("X\n");
    
    return 0;
}