/*
* File: pluck_n_drop.c
* Author: William "Amos" Confer
*
* Description:
* Judging program for the programming competition.  Expects a test data
* file as a command line argument and solution commands from stdin.
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

#define MAX_STACKS 100

// ---------- SDL stuff ---------------------- //
#define SWIDTH 640
#define SHEIGHT 480
#define MARGIN_BOTTOM 30

#include <SDL2/SDL.h>

SDL_Window *window = NULL;
SDL_Surface *windowSurface = NULL;

int blockWidth = 50;
int blockWidthDraw = 50;
int blockHeight = 50; // the amount of geometrical space the block takes up
int blockHeightDraw = 50; // the actual height of a block to draw (to leave a gap)

int delayAmount = 50;

bool InitSDL(void)
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        fprintf(stderr, "Failed to init SDL!\n");
        return false;
    }


    //SDL_SetHint(SDL_HINT_NO_SIGNAL_HANDLERS, "1");
    //SDL_SetHint(SDL_HINT_WINDOWS_ENABLE_MESSAGELOOP, "1");
    //SDL_SetHint(SDL_HINT_VIDEO_X11_NET_WM_PING, "0");


    window = SDL_CreateWindow(
        "Pluck n Drop", SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED, SWIDTH, 
        SHEIGHT, SDL_WINDOW_SHOWN
    );

    if (!window) {
        fprintf(stderr, "Failed to create window\n");
        SDL_Quit();
        return false;
    }

    windowSurface = SDL_GetWindowSurface(window);

    return true;
}

bool UpdateWindow(int stacks[MAX_STACKS], int numStacks, int target[MAX_STACKS], int pos, bool holding)
{
    SDL_Rect r;

    SDL_FillRect(windowSurface, NULL, SDL_MapRGB(windowSurface->format, 0, 0, 0));

    int x, ycur, ytarg, i, j;
    for (x = 0, i = 0; i < numStacks; i++, x += blockWidth)
    {
        ycur = SHEIGHT - MARGIN_BOTTOM - blockHeight;
        ytarg = ((SHEIGHT - MARGIN_BOTTOM) / 2) - blockHeight;

        /* Draw the current stack formation */
        for (j = 0; j < stacks[i]; j++)
        {
            r.x = x; r.y = ycur; r.w = blockWidthDraw; r.h = blockHeightDraw;
            SDL_FillRect(windowSurface, &r, SDL_MapRGB(windowSurface->format, 0, 0, 255));
            ycur -= blockHeight;
        }

        /* Draw the target stack formation */
        for (j = 0; j < target[i]; j++)
        {
            r.x = x; r.y = ytarg; r.w = blockWidthDraw; r.h = blockHeightDraw;
            SDL_FillRect(windowSurface, &r, SDL_MapRGB(windowSurface->format, 0, 150, 0));
            ytarg -= blockHeight;
        }
    }

    /* Draw a square on the bottom left of the screen if we've picked up a block */
    if (holding) {
        r.x = 0;
        r.y = SHEIGHT-10;
        r.w = 10;
        r.h = 10;
        SDL_FillRect(windowSurface, &r, SDL_MapRGB(windowSurface->format, 0, 255, 255));
    }

    /* Draw a square at our current position */
    r.x = (pos * blockWidth);
    r.y = SHEIGHT - 20;
    r.w = blockWidthDraw;
    r.h = 20;

    SDL_FillRect(windowSurface, &r, SDL_MapRGB(windowSurface->format, 255, 0, 0));

    SDL_UpdateWindowSurface(window);
    
    SDL_Delay(delayAmount);

    return true;
}
// ------------------------------------------- //

int main(int argc, char *argv[]) {

    int stacks[MAX_STACKS] = { 0 };
    int target[MAX_STACKS] = { 0 };

    int pos = 0;
    long operation_count = 0;

    long score;
    int stack_count;
    bool holding = false;
    int i;
    char c;

    char str[] = "Your score is: ";

    FILE *f;

    if (argc != 4) {
        printf("\n\nUsage: %s <testfile> <delay_amount> <block_height>\n", argv[0]);
        printf("\n\nExample:\n");
        printf("  mysolverprogram testfile1.txt | %s testfile1.txt 10 15\n", argv[0]);
        return EXIT_SUCCESS;
    }

    // --------------- SDL --------------------- //
    if (!InitSDL()) return EXIT_FAILURE;
    // ----------------------------------------- //

    f = fopen(argv[1], "r");
    if (!f) {
        printf("YIKES!!! opening %s. Errno %d: %s\n", argv[1], errno, strerror(errno));
        exit(EXIT_FAILURE);
    }

    fscanf(f, "%d", &stack_count);

    for (i = 0; i < stack_count; i++) {
        fscanf(f, "%d", &(stacks[i]));
    }

    for (i = 0; i < stack_count; i++) {
        fscanf(f, "%d", &(target[i]));
    }

    fclose(f);

    delayAmount = atoi(argv[2]);
    blockHeight = atoi(argv[3]);
    blockHeightDraw = blockHeight - 3;

    blockWidth = SWIDTH / stack_count;
    blockWidthDraw = blockWidth - 3;
    
    UpdateWindow(stacks, stack_count, target, pos, holding);
    
    SDL_Event dummy;
    do {

        while (SDL_PollEvent(&dummy));

        operation_count++;
        scanf("%c", &c);
        switch (c) {
        case 'L':
            if (pos > 0) {
                pos--;
            }
            break;
        case 'R':
            if (pos < (stack_count - 1)) {
                pos++;
            }
            break;
        case 'P':
            if (!holding && stacks[pos] > 0) {
                stacks[pos]--;
                holding = true;
            }
            break;
        case 'D':
            if (holding) {
                stacks[pos]++;
                holding = false;
            }
            break;
        default:
            operation_count--;
            break;
        }

        UpdateWindow(stacks, stack_count, target, pos, holding);

    } while (c != 'X');

    score = operation_count;
    for (i = 0; i < stack_count; i++) {
        long dif = stacks[i] - target[i];
        dif = dif >= 0 ? dif : -dif;
        score += dif * dif * dif;
    }

    printf("%s%ld\n", str, score);

    SDL_DestroyWindow(window);
    SDL_Quit();

    return EXIT_SUCCESS;
}
